:keyboard:


<br>

# …Netiqette
### ∵ (‥) ∴ ☛ a44 ≛ 44-1 ≛ ₄₃Tc ≛ Te.g. ≛ get :popcorn:

* `do✓` add; dad; dod
* `mo✓` dom; mod
* `✓is` vanilla; v il/ ^an \ₗₐ; vis

Qua<sup>**simodo**</sup> nur halt rückwärts…


### alternativ mainstream :8ball:

|o :repeat_one: |r :checkered_flag: |f :leftwards_arrow_with_hook: |
|--- |-- |-- |
|↑⁵ | |[f]ₘ |
|f ᵐ<sup>→</sup>4 |f 4ₘ | |
|fm4 | f ᵘᵑ 4 | f ④ |


### common lifetime scale :radio:

- dope story `40` invest venture capital _&nbsp;« Voraussetzung bewiesen_  
- meet results `70` reallign asset valuation _&nbsp;« Strategie umgewichten_


# Glossar :information_source:

:pregnant_woman: [Symbole — Sprachregelung](https://gitlab.com/xcheck/fellows/-/blob/main/pool/0×UTF-8.md)


---
[Link ∷ README](/README.md)\
[Link ∷ how2sync](/how2sync.md)\
**[Link ∷ netiqette](/netiqette.md)**
